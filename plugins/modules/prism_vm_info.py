#!/usr/bin/python

# Copyright: (c) 2021, Ross Davies <davies.ross@gmail.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function
__metaclass__ = type

DOCUMENTATION = r'''
    name: prism_vm_info
    plugin_type: module
    short_description: Gather virtual machine information from either Nutanix Prism Element or Nutanix Prism Central.
    description:
        - This module will return the configuration of an existing virtual machine.
    author:
        - Ross Davies <davies.ross@gmail.com>
    version_added: 0.0.9
    requirements:
        - "python >= 2.7"
        - "ntnx-api >= 1.1.30"
    notes:
        - Ensure that the user account used to connect to Prism Element or Prism Central has the appropriate rights to query the VM.
        - Although VM names do not have on each Nutanix cluster, this module assumes that within each individual cluster there is only one instance of each virtual machine name.
    options:
        prism:
            description:
                - The IP address or hostname for either Nutanix Prism Element or Nutanix Prism Central to connect.
            type: str
            required: True
        port:
            description:
                - The TCP port for either Nutanix Prism Element or Nutanix Prism Central to connect.
            type: int
            required: False
        username:
            description:
                - The username for either Nutanix Prism Element or Nutanix Prism Central for authentication.
            type: str
            required: False
        password:
            description:
                - The password for either Nutanix Prism Element or Nutanix Prism Central for authentication.
            type: str
            required: True
        validate_certs:
            type: str
            choices: ['yes', 'no']
            default: 'yes'
        cluster_name:
            description:
                - If connecting to an instance of Prism Central this field is mandatory.
                - Set to the name of a Prism Element cluster where the virtual machine exists.
            type: str
            required: False
        name:
            description:
                - The name of the VM to get info on.
                - This is required if C(uuid) is not supplied.
            type: str
        uuid:
            description:
                - The UUID of the VM to get info on.
                - This is required if C(name) is not supplied.
'''

EXAMPLES = r'''
'''

RETURN = r'''
    vm_info:
        description: metadata about the VM.
        returned: always
        type: dict
        sample: {
        "allow_live_migrate": true,
        "gpus_assigned": false,
        "ha_priority": 0,
        "host_uuid": "c60c1dea-f858-4ee5-ba76-6701819f2837",
        "memory_mb": 4096,
        "name": "gitlb_runner",
        "num_cores_per_vcpu": 1,
        "num_vcpus": 2,
        "power_state": "on",
        "timezone": "America/Phoenix",
        "uuid": "2afd6b6a-e55a-489d-99d3-0ed553e6616d",
        "vm_disk_info": [
            {
                "disk_address": {
                    "device_bus": "ide",
                    "device_index": 0,
                    "disk_label": "ide.0"
                },
                "is_cdrom": true,
                "is_empty": true,
                "flash_mode_enabled": false,
                "is_scsi_passthrough": true,
                "is_hot_remove_enabled": false,
                "is_thin_provisioned": false,
                "shared": false
            },
            {
                "disk_address": {
                    "device_bus": "scsi",
                    "device_index": 0,
                    "disk_label": "scsi.0",
                    "vmdisk_uuid": "d19747af-1f1e-4abd-8f24-89c7e52ba375"
                },
                "is_cdrom": false,
                "is_empty": false,
                "flash_mode_enabled": false,
                "is_scsi_passthrough": true,
                "is_hot_remove_enabled": true,
                "is_thin_provisioned": false,
                "shared": false,
                "source_disk_address": {
                    "vmdisk_uuid": "1841fdc7-a095-48e0-ba46-13641c341cf8"
                },
                "storage_container_uuid": "4c1979c1-8b56-4db8-aa3c-8e2906344d5d",
                "size": 214748364800
            }
        ],
        "vm_features": {
            "AGENT_VM": false,
            "VGA_CONSOLE": true
        },
        "vm_logical_timestamp": 15,
        "vm_nics": [
            {
                "mac_address": "50:6b:8d:c0:c0:53",
                "network_uuid": "fe48a7d5-5fce-4c8b-b895-68c11e93b81c",
                "model": "",
                "ip_address": "192.168.1.119",
                "is_connected": true
            }
        ]
    }
'''

import traceback

try:
    from ntnx_api.client import PrismApi
    from ntnx_api import prism
    HAS_NTNX_API = True
except ImportError:
    NTNX_API_IMP_ERR = traceback.format_exc()
    HAS_NTNX_API = False

from ansible.module_utils.basic import AnsibleModule, missing_required_lib
from ansible_collections.grdavies.nutanix.plugins.module_utils.common import prism_argument_spec


def main():
    """
    Main function of the module.
    """
    argument_spec = prism_argument_spec()
    argument_spec.update(
        state=dict(default='present', choices=['present', 'absent', 'on', 'off', 'guest-reboot', 'guest-shutdown', 'reset', 'powercycle', 'resume', 'suspend']),
        name=dict(type='str'),
        uuid=dict(type='str'),
        cluster_name=dict(required=False, type='str', default=None),
    )

    module = AnsibleModule(
        argument_spec=argument_spec,
        required_if=[
        ],
        mutually_exclusive=[
            ['name', 'uuid'],
        ],
        required_one_of=[
            ['name', 'uuid'],
        ],
        supports_check_mode=True
    )

    if not HAS_NTNX_API:
        module.fail_json(msg=missing_required_lib("ntnx-api"), exception=NTNX_API_IMP_ERR)

    is_error = False
    result = {
        'changed': False,
        'original_message': '',
        'message': '',
        'vm_info': {
        },
    }

    validate_certs_bool = False

    # Prism Connection Options
    prism_ip = module.params.get('prism')
    username = module.params.get('username')
    password = module.params.get('password')
    port = module.params.get('port')
    validate_certs = module.params.get('validate_certs')

    # Module Options
    cluster_name = module.params.get('cluster_name')
    name = module.params.get('name')
    uuid = module.params.get('uuid')

    # check API connection
    if validate_certs == 'yes':
        validate_certs_bool = True

    api_client = PrismApi(ip_address=prism_ip, username=username, password=password, port=port, validate_certs=validate_certs_bool)
    if not api_client.test():
        is_error = True
        result['message'] = "Connection to Prism Endpoint failed. {0}".format(api_client)

    # require cluster_name if connecting to a prism central instance
    if api_client.connection_type == 'pc' and not cluster_name:
        is_error = True
        result['message'] = 'When making a change on a multi-cluster environment ie. Prism Central, you need to define the name of the cluster where ' \
                            'the change is to occur.'
    if is_error:
        module.fail_json(msg='If connected to PC please provide cluster_name parameter.', **result)

    # loop through clusters
    # if cluster_name provided only process that specific cluster
    cluster_obj = prism.Cluster(api_client=api_client)
    vms_obj = prism.Vms(api_client=api_client)
    cluster_uuids = cluster_obj.get_all_uuids()
    for cluster_uuid in cluster_uuids:
        # result['debug'].append('working on cluster "{0}"'.format(cluster_uuids))
        process_cluster = False
        cluster = cluster_obj.get(cluster_uuid)
        if cluster_name:
            if cluster_name.lower() == cluster.get('name').lower():
                process_cluster = True
        else:
            process_cluster = True

        if process_cluster:
            if name:
                try:
                    result = vms_obj.search_name(name=name, clusteruuid=cluster_uuid, refresh=True)

                except Exception as err:
                    is_error = True
                    result['message'] = 'VM search failed. Name: {0}. Error detail: {1}'.format(name, err)

            elif uuid:
                try:
                    result = vms_obj.search_uuid(uuid=uuid, clusteruuid=cluster_uuid, refresh=True)

                except Exception as err:
                    is_error = True
                    result['message'] = 'VM search failed. Uuid: {0}. Error detail: {1}'.format(uuid, err)


    if is_error:
        module.fail_json(msg=result['message'], **result)

    else:
        module.exit_json(**result)


if __name__ == '__main__':
    main()
